// Маска телефонов
$(document).ready(function() {
	$('input[type="tel"]').mask('+7 (999) 999-99-99');
});
// Pop up window
$(document).ready(function() {
	// Popup opening
	$('.open-popup').click(function (e) {
		e.preventDefault();
		var $pop_up_f = ('#' + $(this).attr('data-win')),
			$popup_size = $(this).attr('data-size'),
			$popup = $($pop_up_f),
			$cur_height = $popup.height(),
			$cur_width = $popup.width(),
			$win_height = $(window).height(),
			$form = $popup.find('.w-form'),
			$form_id = $form.attr('id'),
			$link_id = $(this).attr('id'),
			$summary_id = ($form_id + '_' + $link_id);
		// $form.prop('id', $summary_id);
		$('.w-overlay').fadeIn();
		if ($popup_size == 'full') {
			$($pop_up_f).fadeIn(200, function() {
				$(this).css({'top': '0', 'margin-left': -($cur_width/2), 'margin-top': 0}).animate({'top': 0});
			});
		} else {
			$($pop_up_f).show().css({'top': -$cur_height, 'margin-left': -($cur_width/2), 'margin-top': -($cur_height/2)}).animate({'top': ($win_height/2)});
		}
	});
	// Popup closing
	$('.w-overlay, .w-popup__close').click(function(e){
		e.preventDefault();
		 if ($('.w-popup').is(':visible')) {
			$('.w-overlay, .w-popup').fadeOut('100');
		}
	});
});

/*$(document).ready(function() {
	// Валидация форм
	$.validate({
		validateOnBlur : true,
		showHelpOnFocus : false,
		addSuggestions : false,
		scrollToTopOnError : false,
		borderColorOnError : '#d71818',
		form : '#s-promo-form',
		onSuccess : function(form) {
			var m_data = form.serialize();
			$.ajax({
				type: "POST",
				url: 'js/mail.php',
				data: m_data,
				success: showThanks()
			});
			return false;
		}
	});
});*/