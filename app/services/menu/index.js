'use strict';

module.exports = /*@ngInject*/ function($rootScope) {
	return {
		toggleMenu: function() {
			$rootScope.IS_MENU_OPEN = !$rootScope.IS_MENU_OPEN;
			$('.header__menu-icon').toggleClass('header__menu-icon--opened header__menu-icon--closed');
			$('.sidebar').toggleClass('sidebar--opened sidebar--closed');
			$('.header__menu').toggleClass('header__menu--closed header__menu--opened');
			$('.arch').toggleClass('arch--moved');
			$('.header').toggleClass('header--moved');
			$('html').toggleClass('noscroll');
		}
	};
};