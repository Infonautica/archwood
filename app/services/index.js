'use strict';

module.exports = angular.module('app.servicesModule', [])
	.factory('lightbox', require('./lightbox'))
	.factory('menu', require('./menu'));
