'use strict';

module.exports = /*@ngInject*/ function() {
	return {
		initLightbox: function() {
			$(document).ready(function() {
				$('.js-lightbox--gallery').each(function() {
					$(this).magnificPopup({
						delegate: 'a',
						type: 'image',
						mainClass: 'mfp-fade',
						closeMarkup: '<div class="mfp-close"></div>',
						gallery: {
							enabled: true,
							navigateByImgClick: true,
							tCounter: ''
						}
					});
				});

				$('.js-lightbox--video').each(function() {
					$(this).magnificPopup({
						type: 'iframe',
						mainClass: 'mfp-fade',
						closeMarkup: '<div class="mfp-close"></div>'
					});
				});
			});
		}
	};
};