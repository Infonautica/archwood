'use strict';

module.exports = angular.module('app.jobs', [])
    .directive('jobs', require('./widget'));
