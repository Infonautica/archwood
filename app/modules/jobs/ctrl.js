'use strict';

module.exports = /*@ngInject*/ function($scope, $rootScope) {
    $scope.toggleJob = function($event) {
        var self = $event.currentTarget;
        var jobElement = $(self).parents('.job');
        var jobContent = jobElement.find('.job__content');

        jobContent.slideToggle(200, function() {
            jobElement.toggleClass('job--opened job--closed');
        });
    };

    $scope.sendForm = function($event) {
        // Form and serialized data
        var vacancieForm = $event.target.form;
        var m_data = $(vacancieForm).serialize();

        // Dropzone in form
        //var currentDropzone = vacancieForm.getElementsByClassName('job__dropzone')[0];
        //document.getElementsByClassName('job__dropzone')[0].__proto__.processQueue;

        // Ajax request
        $.ajax({
            type: "POST",
            url: '/vacancie/send',
            data: m_data,
            success: function() {
                $rootScope.popups.showPopup('popup--thank', true);
            }
        });
    };

    $scope.attachString = $rootScope.currLang == 'ru' ? '+ Прикрепить резюме или портфолио' : '+ Attach CV or portfolio';

    $scope.dropzoneConfig = {
        anyFileAttached: false,
        'options': { // passed into the Dropzone constructor
            url: '/vacancie/send',
            method: 'post',
            autoDiscover: false,
            addRemoveLinks: 'dictRemoveFile',
            paramName: "dropFile",
            autoProcessQueue: false,
            uploadMultiple: true,
            parallelUploads: 100,
            maxFiles: 100,
            previewsContainer: '.job__dropzone',
            clickable: '.job__dropzone'
        },
        'eventHandlers': {
            'addedfile': function() {
                this.anyFileAttached = true;
                $(".job__dropzone").attr("data-content", $scope.attachString);
            },
            'removedfile': function() {
                var fileCount = $(".job__dropzone .dz-preview").length;
                if (fileCount <= 0) {
                    $(".job__dropzone").attr("data-content", $scope.attachString);
                }
            },
            'success': function(file, res) {
                console.log(res);
                alert(res);
                $rootScope.popups.showPopup('popup--thank', true);
            }
        }
    };
};