'use strict';

module.exports = angular.module('app.service', [])
    .directive('service', require('./widget'));
