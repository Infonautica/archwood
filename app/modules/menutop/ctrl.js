'use strict';

module.exports = /*@ngInject*/ function($scope, $state, $document) {

    $scope.toTop = function() {
        var promovideo = document.getElementsByClassName('promo-video');
        $document.scrollToElement(promovideo, 0, 800);
    };

    // $scope.toggleMenuType = function(){
    //     $('.header').toggleClass('header--white');
    // };

    // $scope.lastScrollTop = 0;

    // $scope.initHeader = function() {
    //     $(window).on('scroll', function() {
    //         var distanceY = window.pageYOffset || document.documentElement.scrollTop,
    //             shrinkOn = 0,
    //             headerClass = 'header--white',
    //             header = $('.header');

    //         if ($state.current.name == 'index') {
    //             shrinkOn = $('.promo').height();
    //         } else if ($state.current.name == 'service') {
    //             shrinkOn = $('.service').height();
    //         } else {
    //             shrinkOn = 0;
    //         }

    //         if (distanceY > shrinkOn) {
    //             header.addClass(headerClass);

    //             /* Hide menu */
    //             if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
    //                 if (distanceY < $scope.lastScrollTop || distanceY === 0){
    //                     header.removeClass('header--hidden');
    //                 } else {
    //                     header.addClass('header--hidden');
    //                 }
    //                 $scope.lastScrollTop = distanceY;
    //             }
    //         } else {
    //             if (header.hasClass(headerClass)) {
    //                 header.removeClass(headerClass);
    //             }
    //         }
    //     });
    // }();

     $scope.toggleLang = function() {
         $('.header__languages-elem').toggleClass('header__languages-elem--active');
     };
};