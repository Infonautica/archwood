'use strict';

module.exports = angular.module('app.menutop', [])
    .directive('menuTop', require('./widget'));
