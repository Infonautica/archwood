'use strict';

module.exports = angular.module('app.about', [])
    .directive('about', require('./widget'));
