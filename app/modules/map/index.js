'use strict';

module.exports = angular.module('app.map', [])
    .directive('map', require('./widget'));
