'use strict';

module.exports = /*@ngInject*/ function($scope, $rootScope) {

    $scope.setMarkers = function setMarkers(map, locations, infowindows){
        var marker, i;

        for (i = 0; i < locations.length; i++) {
            var title = locations[i].title;
            var long = locations[i].address.geo[0];
            var lat = locations[i].address.geo[1];
            var baloon = '<p class="map__baloon-heading">' + title + '</p>' + '<p class="map__baloon-text">' + $rootScope.phones.contact.text + '</p>' + '<p class="map__baloon-text">Адрес: ' + locations[i].address.street1 + '</p>' + '<p class="map__baloon-text map__baloon-text--description" translate="MAP__DESCR">' + locations[i].description + '</p>';

            var latlngset = new google.maps.LatLng(lat, long);
            var pinImageUrl = '/img/elem/map__pin.png';
            var marker = new google.maps.Marker({
                map: map,
                title: title ,
                position: latlngset,
                icon: pinImageUrl
            });
            map.setCenter(marker.getPosition());

            var content = '<div class="map__baloon">' + baloon + '</div>';
            var infowindow = new google.maps.InfoWindow({
                maxWidth: 400
            });
            infowindows.push(infowindow);

            google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){
                return function() {
                    for (var j = 0; j < infowindows.length; j++) {
                        infowindows[j].close();
                    }
                    infowindow.close();
                    infowindow.setContent(content);
                    infowindow.open(map,marker);
                };
            })(marker,content,infowindow));
        }
    };

    $scope.initialize = function() {
        angular.element(document).ready(function() {
            $('#map__element').ready(function() {
                var styles = [
                    {
                        stylers: [
                            { hue: "#FFFFFF" },
                            { saturation: -100 },
                            { lightness: -2 }
                        ]
                    },{
                        featureType: "road",
                        elementType: "geometry",
                        stylers: [
                            { lightness: 3 },
                            { visibility: "simplified" }
                        ]
                    },{
                        featureType: "road",
                        elementType: "labels",
                        stylers: [
                            { visibility: "off" }
                        ]
                    },{
                        featureType: "all",
                        elementType: "labels",
                        stylers: [
                            { visibility: "simplified" }
                        ]
                    }
                ];

                var contactsMap__options = {
                    zoom: 12,
                    center: new google.maps.LatLng("55.579037", "37.615587"),
                    scrollwheel: false,
                    mapTypeControl: false,
                    navigationControl: false
                };

                var contactsMap = new google.maps.Map(document.getElementById('map__element'), contactsMap__options);
                var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});
                contactsMap.mapTypes.set('map_style', styledMap);
                contactsMap.setMapTypeId('map_style');
                $scope.setMarkers(contactsMap, $scope.locations, $scope.infoWindows);
            });
        });
    }();
};