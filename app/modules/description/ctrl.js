'use strict';

module.exports = /*@ngInject*/ function($scope) {

    $scope.toggleDescription = function($event) {
        var self = $event.currentTarget;
        var cut = $(self).parents('.description').find('.description__cut');

        cut.slideToggle(200, function() {
            if (cut.is(':visible')) {
                $(self).html('Свернуть описание');
            } else {
                $(self).html('Показать полностью');
            }

            $(self).toggleClass('description__more--to-display description__more--to-hide');
        });
    };
};