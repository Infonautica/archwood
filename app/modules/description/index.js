'use strict';

module.exports = angular.module('app.description', [])
    .directive('description', require('./widget'));
