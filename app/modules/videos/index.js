'use strict';

module.exports = angular.module('app.videos', [])
    .directive('videos', require('./widget'));
