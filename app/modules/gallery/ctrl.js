'use strict';

module.exports = /*@ngInject*/ function($scope) {

    $scope.centerArea = (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) ? 0.8 : 0.5;

    $scope.galleryInit = function() {
        $('#gallery__gallery').royalSlider({
            addActiveClass: true,
            arrowsNav: false,
            controlNavigation: 'none',
            autoScaleSlider: true,
            autoScaleSliderWidth: 960,
            autoScaleSliderHeight: 340,
            loop: true,
            fadeinLoadedSlide: false,
            globalCaption: true,
            keyboardNavEnabled: true,
            globalCaptionInside: true,

            visibleNearby: {
                enabled: true,
                centerArea: $scope.centerArea,
                center: true,
                breakpoint: 315,
                breakpointCenterArea: 0.9,
                navigateByCenterClick: true
            }
        });
    };

    $scope.$on('$viewContentLoaded', function() {
        // setTimeout($scope.galleryInit, 500);
        angular.element(document).ready(function() {
            $scope.galleryInit();
        });
    });
};