'use strict';

module.exports = angular.module('app.gallery', [])
    .directive('gallery', require('./widget'));
