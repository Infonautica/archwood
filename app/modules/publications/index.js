'use strict';

module.exports = angular.module('app.publications', ['app.servicesModule'])
    .directive('publications', require('./widget'));
