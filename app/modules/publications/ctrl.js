'use strict';

module.exports = /*@ngInject*/ function($scope, lightbox) {
    lightbox.initLightbox();

    $scope.initCarousel = function() {
        $('.publications__carousel').owlCarousel({
            loop: true,
            items: 6,
            margin: 10,
            responsive: {
                0: {
                    items:3
                },
                480: {
                    items:4
                },
                768: {
                    items:6
                }
            }
        });
    }();
};