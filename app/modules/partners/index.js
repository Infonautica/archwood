'use strict';

module.exports = angular.module('app.partners', [])
    .directive('partners', require('./widget'));
