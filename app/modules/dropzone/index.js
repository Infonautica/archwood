'use strict';

module.exports = angular.module('app.dropzone', [])
    .directive('dropzone', require('./widget'));
