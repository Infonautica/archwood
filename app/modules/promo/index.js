'use strict';

module.exports = angular.module('app.promo', [])
    .directive('promo', require('./widget'));
