'use strict';

module.exports = /*@ngInject*/ function($scope) {

    /*
     * Promo scroll
     * */
    $scope.arrowDown = function() {
        var offset = $('#works')[0].offsetTop;
        var headerHeight = $('#header').height();
        $('html, body').stop().animate({
            scrollTop: offset
        }, 400);
    };

    $scope.initSlider = function() {
        $(".promo__slider").responsiveSlides({
            auto: true,             // Boolean: Animate automatically, true or false
            speed: 500,            // Integer: Speed of the transition, in milliseconds
            timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
            pager: true,           // Boolean: Show pager, true or false
            nav: true,             // Boolean: Show navigation, true or false
            random: false,          // Boolean: Randomize the order of the slides, true or false
            pause: false,           // Boolean: Pause on hover, true or false
            pauseControls: false,    // Boolean: Pause when hovering controls, true or false
            prevText: "Previous",   // String: Text for the "previous" button
            nextText: "Next",       // String: Text for the "next" button
            maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
            navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
            manualControls: "",     // Selector: Declare custom pager navigation
            namespace: "rslides"   // String: Change the default namespace used
        });

        $('body').on('keydown', function(e) {
            if (e.keyCode == 37) {
                $('.rslides1_nav.prev').trigger('click');
            } else if (e.keyCode == 39) {
                $('.rslides1_nav.next').trigger('click');
            }
        });
    }();
};