'use strict';

module.exports = /*@ngInject*/ function($scope, lightbox) {

    /*
    * Initializing lightbox
    * */
    lightbox.initLightbox();


    /*
    * Popup carousel: init, navigation and titles
    * */
    $scope.initPopupCarousel = function() {
        var popupCarousel = $('.w-popup__gallery-carousel');
        popupCarousel.slick({
            infinite: true,
            slidesToShow: 1,
            arrows: false
        });

        $('.w-popup__gallery-nav--prev').click(function() {
            popupCarousel.slick('slickPrev');
        });

        $('.w-popup__gallery-nav--next').click(function() {
            popupCarousel.slick('slickNext');
        });

        popupCarousel.on('afterChange', function() {
            var tittleContent = $('.slick-current').find('.w-popup__gallery-slide').attr('alt');
            $('.w-popup__gallery-title').text(tittleContent);
        });
    }();


    /*
    * Popup dropzone config
    * */
    $scope.dropzoneConfig = {
        anyFileAttached: false,
        'options': { // passed into the Dropzone constructor
            'url': 'upload.php',
            autoDiscover: false,
            addRemoveLinks: 'dictRemoveFile',
            paramName: "dropFile",
            autoProcessQueue: false,
            uploadMultiple: true,
            parallelUploads: 100,
            maxFiles: 100,
            previewsContainer: '.w-popup__dropzone',
            clickable: '.w-popup__dropzone'
        },
        'eventHandlers': {
            'addedfile': function() {
                this.anyFileAttached = true;
                $(".w-popup__dropzone").attr("data-content", "+ Прикрепить еще");
            },
            'removedfile': function() {
                var fileCount = $(".w-popup__dropzone .dz-preview").length;
                if (fileCount <= 0) {
                    $(".w-popup__dropzone").attr("data-content", "+ Прикрепить файл");
                }
            }
        }
    };
};