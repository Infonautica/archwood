'use strict';

module.exports = angular.module('app.modal', ['app.servicesModule'])
    .directive('modal', require('./widget'));
