'use strict';

module.exports = function() {
    return {
        restrict: 'E',
        template: require('./view.html'),
        controller: require('./ctrl')
    };
};