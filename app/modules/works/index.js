'use strict';

module.exports = angular.module('app.works', [])
    .directive('works', require('./widget'));
