'use strict';

module.exports = angular.module('app.projects', [])
    .directive('projects', require('./widget'));
