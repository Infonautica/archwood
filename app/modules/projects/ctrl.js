'use strict';

module.exports = /*@ngInject*/ function($scope, $state, $rootScope) {

    /*
    * Variable to change template
    * */
    $scope.showInter = $state.params.key == 'interery';
    $scope.showSupport = $state.params.key == 'soprovozhdenie-remonta';
    $scope.showConsult = $state.params.key == 'konsultirovanie';

    /*
    * Action dates
    * */
    $scope.dates = {
        day: 0,
        month: ''
    };

    $scope.setDates = function() {
        var currentDate = new Date();
        var actionDate = new Date();

        actionDate.setDate(currentDate.getDate() + 3);
        $scope.dates.day = actionDate.getDate();
        $scope.dates.month = actionDate.getMonth();

        switch($scope.dates.month) {
            case 0:
                $scope.dates.month = 'января';
                $scope.dates.monthENG = 'january';
                break;
            case 1:
                $scope.dates.month = 'февраля';
                $scope.dates.monthENG = 'february';
                break;
            case 2:
                $scope.dates.month = 'марта';
                $scope.dates.monthENG = 'march';
                break;
            case 3:
                $scope.dates.month = 'апреля';
                $scope.dates.monthENG = 'april';
                break;
            case 4:
                $scope.dates.month = 'мая';
                $scope.dates.monthENG = 'may';
                break;
            case 5:
                $scope.dates.month = 'июня';
                $scope.dates.monthENG = 'june';
                break;
            case 6:
                $scope.dates.month = 'июля';
                $scope.dates.monthENG = 'jule';
                break;
            case 7:
                $scope.dates.month = 'августа';
                $scope.dates.monthENG = 'august';
                break;
            case 8:
                $scope.dates.month = 'сентября';
                $scope.dates.monthENG = 'septemper';
                break;
            case 9:
                $scope.dates.month = 'октября';
                $scope.dates.monthENG = 'october';
                break;
            case 10:
                $scope.dates.month = 'ноября';
                $scope.dates.monthENG = 'november';
                break;
            case 11:
                $scope.dates.month = 'декабря';
                $scope.dates.monthENG = 'december';
                break;
        }
    }();
};