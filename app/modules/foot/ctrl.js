'use strict';

module.exports = /*@ngInject*/ function($scope, $state) {
    $scope.goToPortfolio = function(page, eID, type) {
        var params = {
            type: type ? type : 'Все проекты',
            style: 'Все стили'
        };

        $state.go(page, params).then(function() {
            var targetElem = $('#arch').find('#' + eID)[0];
            setTimeout(function() {
                var offset = targetElem.offsetTop;
                if (page == 'about' && eID == 'partners') {
                    offset = (offset - 110);
                }
                $('html, body').stop().animate({
                    scrollTop: offset
                }, 400);
            }, 500);
        });
    };
};