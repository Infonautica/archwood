'use strict';

module.exports = angular.module('app.foot', ['duScroll'])
    .directive('foot', require('./widget'));
