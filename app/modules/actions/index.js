'use strict';

module.exports = angular.module('app.actions', [])
    .directive('actions', require('./widget'));
