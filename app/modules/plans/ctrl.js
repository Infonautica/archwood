'use strict';

module.exports = /*@ngInject*/ function($scope, lightbox) {
    lightbox.initLightbox();
};