'use strict';

module.exports = angular.module('app.plans', ['app.servicesModule'])
    .directive('plans', require('./widget'));
