'use strict';

module.exports = /*@ngInject*/ function($scope, $document, $state, $rootScope, menu) {
    $scope.toggleMenu = function() {
        menu.toggleMenu();
    };

    $scope.toPortfolio = function() {
        if ($rootScope.IS_MENU_OPEN) {
            menu.toggleMenu();
        }
        $state.go('index').then(function() {
            var portfolio = document.getElementById('works');
            var offset = document.getElementsByClassName('header__container')[0].offsetHeight;
            $document.scrollToElement(portfolio, offset, 800);
        });
    };
};