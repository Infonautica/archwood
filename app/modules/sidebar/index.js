'use strict';

module.exports = angular.module('app.sidebar', ['app.servicesModule', 'duScroll'])
    .directive('sidebar', require('./widget'));
