'use strict';

module.exports = angular.module('app.supervision', [])
    .directive('supervision', require('./widget'));
