'use strict';

module.exports = angular.module('app.contacts', [])
    .directive('contacts', require('./widget'));
