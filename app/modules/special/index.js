'use strict';

module.exports = angular.module('app.special', [])
    .directive('special', require('./widget'));
