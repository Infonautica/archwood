'use strict';

module.exports = /*@ngInject*/ function($scope, $state) {
    $scope.moreInfo = function(page, eID) {
        var params = {
            key: 'soprovozhdenie-remonta'
        };

        $state.go(page, params).then(function() {
            var targetElem = $('#arch').find('#' + eID)[0];
            angular.element(document).ready(function() {
                var offset = targetElem.offsetTop;
                if (page == 'about' && eID == 'partners') {
                    offset = (offset - 110);
                }
                $('html, body').stop().animate({
                    scrollTop: offset - $('#header').height()
                }, 400);
            });
        });
    };
};