'use strict';

module.exports = /*@ngInject*/ function($scope, $rootScope) {
    $scope.additionalTrigger = function($event) {
        var content = $event.target.innerText;
        var USE_LINK = (content == 'Авторский надзор' || content == 'Сопровождение ремонта');

        if (USE_LINK) {
            $rootScope.goToHash('service', 'special');
        } else {
            var stepName;

            switch(content) {
                case 'Планировочные решения' || 'Layout solutions': stepName = 'popup--project__step1'; break;
                case 'Layout solutions': stepName = 'popup--project__step1'; break;

                case 'Sketches': stepName = 'popup--project__step2'; break;
                case 'Эскизы от руки' || 'Sketches': stepName = 'popup--project__step2'; break;

                case '3D-visualization': stepName = 'popup--project__step3'; break;
                case '3Д Визуализация' || '3D-visualization': stepName = 'popup--project__step3'; break;

                case 'Design drawing': stepName = 'popup--project__step4'; break;
                case 'Рабочие чертежи' || 'Design drawing': stepName = 'popup--project__step4'; break;

                case 'Estimate': stepName = 'popup--project__step5'; break;
                case 'Смета' || 'Estimate': stepName = 'popup--project__step5'; break;

                default: stepName = 'popup--project__step1'; break;
            }

            $rootScope.popups.showPopup('popup--project', false);
            $rootScope.popups.scrollToStep(stepName);
        }
    };
};