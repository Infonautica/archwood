'use strict';

module.exports = angular.module('app.additional', [])
    .directive('additional', require('./widget'));
