'use strict';

module.exports = angular.module('app.sketches', ['app.servicesModule'])
    .directive('sketches', require('./widget'));
