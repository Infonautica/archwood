'use strict';

module.exports = /*@ngInject*/ function($scope, lightbox) {
    lightbox.initLightbox();

    $scope.initSketches = function() {
        setTimeout(function() {
            var sketchesGallery = new Freewall("#sketches__gallery");
            sketchesGallery.reset({
                selector: '.sketches__gallery-item',
                animate: false,
                cellW: 470,
                cellH: 20,
                minWidth: 320,
                gutterY: 20,
                gutterX: 20,
                onResize: function() {
                    sketchesGallery.fitWidth();
                }
            });
            sketchesGallery.fitWidth();
            $(window).trigger("resize");
        }, 1000);
    }();
};