'use strict';

module.exports = /*@ngInject*/ function($stateProvider, $urlRouterProvider, $locationProvider, $translateProvider, $sceProvider, langHolder) {
    $stateProvider
        .state('index', {
            url: "/?{type:string}?{style:string}",
            templateUrl: '/templates/home.html',
            controller: 'pagesIndexShowCtrl',
            //reloadOnSearch: false,
            params: {
                type: 'Все проекты',
                style: 'Все стили'
            }
        })
        .state('project', {
            url: "/project/{projectName:string}",
            templateUrl: '/templates/project.html',
            controller: 'pagesProjectShowCtrl'
        })
        .state('about', {
            url: "/about",
            templateUrl: '/templates/about.html',
            controller: 'pagesAboutShowCtrl'
        })
        .state('contacts', {
            url: "/contacts",
            templateUrl: '/templates/contacts.html',
            controller: 'pagesContactsShowCtrl'
        })
        .state('service', {
            url: "/service?{key:string}",
            templateUrl: function(stateParams) {
                if (stateParams.key == 'soprovozhdenie-remonta' || stateParams.key == 'konsultirovanie') {
                    return '/templates/service2.html';
                } else {
                    return '/templates/service.html';
                }
            },
            controller: 'pagesServiceShowCtrl',
            params: {
                key: 'interery'
            }
        });


    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);

    /*
    * Languages
    * */
    $translateProvider.translations('en', langHolder.en);
    $translateProvider.translations('ru', langHolder.ru);
    $translateProvider.preferredLanguage('ru');
    $translateProvider.useSanitizeValueStrategy(null);

    /*
    * String escaping
    * */
    $sceProvider.enabled(false);
};