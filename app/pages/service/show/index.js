'use strict';

module.exports = angular.module('app.pages.service.show', [])
    .controller('pagesServiceShowCtrl', require('./ctrl'));
