'use strict';

module.exports = /*@ngInject*/ function($scope, $state, $sce, $rootScope) {

    /*
    * Main object with data
    * */
    $scope.servicesContent = {};


    /*
    * Function to get data from server
    * */
    $scope.initialRequest = function() {
        var currentKey = $state.params.key;

        $.ajax({
            async: false,
            method: 'POST',
            url: '/services/' + currentKey,
            success: function(res) {

                // Saving response
                $scope.servicesContent = res;
                $rootScope.pages.services[currentKey] = res;

                /*
                * Using sanitizer
                * */

                if (currentKey == 'interery') {
                    var interiorsKeys = ['description', 'cost', 'action'];
                    interiorsKeys.map(function(key) {
                        $scope.servicesContent.layoutProject[key] = $sce.trustAsHtml(res.layoutProject[key]);
                        $scope.servicesContent.en.layoutProject[key] = $sce.trustAsHtml(res.en.layoutProject[key]);

                        $scope.servicesContent.designProject[key] = $sce.trustAsHtml(res.designProject[key]);
                        $scope.servicesContent.en.designProject[key] = $sce.trustAsHtml(res.en.designProject[key]);
                    });

                    $scope.servicesContent.designerSupervision.description = $sce.trustAsHtml(res.designerSupervision.description);
                    $scope.servicesContent.en.designerSupervision.description = $sce.trustAsHtml(res.en.designerSupervision.description);

                    $scope.servicesContent.repairSupervision.description = $sce.trustAsHtml(res.repairSupervision.description);
                    $scope.servicesContent.en.repairSupervision.description = $sce.trustAsHtml(res.en.repairSupervision.description);
                } else if (currentKey == 'soprovozhdenie-remonta') {
                    var repairKeys = ['all_html', 'before_html'];
                    repairKeys.map(function(key) {
                        $scope.servicesContent.repairSupport[key] = $sce.trustAsHtml(res.repairSupport[key]);
                        $scope.servicesContent.en.repairSupport[key] = $sce.trustAsHtml(res.en.repairSupport[key]);
                    });

                    var repairAfterKeys = ['left', 'right'];
                    repairAfterKeys.map(function(key) {
                        $scope.servicesContent.repairSupport.after_html[key] = $sce.trustAsHtml(res.repairSupport.after_html[key]);
                        $scope.servicesContent.en.repairSupport.after_html[key] = $sce.trustAsHtml(res.en.repairSupport.after_html[key]);
                    });
                } else if (currentKey == 'konsultirovanie') {
                     $scope.servicesContent.consult.description = $sce.trustAsHtml(res.consult.description);
                     $scope.servicesContent.en.consult.description = $sce.trustAsHtml(res.en.consult.description);

                     var consultAdditionalKeys = ['left', 'right'];
                     consultAdditionalKeys.map(function(key) {
                        $scope.servicesContent.consult.additional[key] = $sce.trustAsHtml(res.consult.additional[key]);
                        $scope.servicesContent.en.consult.additional[key] = $sce.trustAsHtml(res.en.consult.additional[key]);
                     });
                }
            }
        });

        /*
        * Send Interiors information to the $rootScope
        * */
        var ruPath = $scope.servicesContent;
        var enPath = $scope.servicesContent.en;
        var isCurrentRU = $rootScope.currLang == 'ru';

        if ($state.params.key == 'interery') {
            var interiorsKeys = ['description', 'cost', 'title', 'subtitle', 'action'];
            interiorsKeys.map(function(key) {
                $rootScope.parts.services.interery.layoutProject[key] = isCurrentRU ? ruPath.layoutProject[key] : enPath.layoutProject[key];
                $rootScope.parts.services.interery.designProject[key] = isCurrentRU ? ruPath.designProject[key] : enPath.designProject[key];
            });
            $rootScope.parts.services.interery.designerSupervision.description = isCurrentRU ? ruPath.designerSupervision.description : enPath.designerSupervision.description;
            $rootScope.parts.services.interery.repairSupervision.description = isCurrentRU ? ruPath.repairSupervision.description : enPath.repairSupervision.description;
        } else if ($state.params.key == 'soprovozhdenie-remonta') {
            var repairKeys = ['all_html', 'before_html'];
            repairKeys.map(function(key) {
                $rootScope.parts.services['soprovozhdenie-remonta'].repairSupport[key] = isCurrentRU ? ruPath.repairSupport[key] : enPath.repairSupport[key];
            });

            var repairAfterKeys = ['left', 'right'];
            repairAfterKeys.map(function(key) {
                $rootScope.parts.services['soprovozhdenie-remonta'].repairSupport.after_html[key] = isCurrentRU ? ruPath.repairSupport.after_html[key] : enPath.repairSupport.after_html[key];
            });

            $rootScope.parts.services['soprovozhdenie-remonta'].repairSupport.common_descr = isCurrentRU ? ruPath.repairSupport.common_descr : enPath.repairSupport.common_descr;

        } else if (currentKey == 'konsultirovanie') {
            var consultAdditionalKeys = ['left', 'right'];
            consultAdditionalKeys.map(function(key) {
                $rootScope.parts.services.konsultirovanie.consult.additional[key] = isCurrentRU ? ruPath.consult.additional[key] : enPath.consult.additional[key];
            });

            $rootScope.parts.services.konsultirovanie.consult.description = isCurrentRU ? ruPath.consult.description : enPath.consult.description;
        }
    }();
};