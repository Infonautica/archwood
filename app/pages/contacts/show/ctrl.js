'use strict';

module.exports = /*@ngInject*/ function($scope, $http) {

    /*
    * Main object with data
    * */
    $scope.locations = [];
    $scope.infoWindows = [];


    /*
    * Function to get data from server
    * */
    $scope.initialRequest = function() {
        $http({
            method: 'POST',
            url: '/offices'
        }).then(function successCallback(response) {
            $scope.locations = response.data;
        }, function errorCallback(response) {
            console.log('Ошибка в получении данных для карты:\n', response);
        });
    }();
};