'use strict';

module.exports = angular.module('app.pages.contacts.show', [])
    .controller('pagesContactsShowCtrl', require('./ctrl'));
