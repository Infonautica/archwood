'use strict';

module.exports = /*@ngInject*/ function($scope, $state, $sce, langHolder, $translate, $rootScope) {

    /*
    * Main object with data
    * */
    $scope.projectContent = {};


    /*
    * Function to get data from server
    * */
    $scope.initialRequest = function() {
        var currentProjectName = $state.params.projectName;
        $.ajax({
            async: false,
            method: 'POST',
            url: '/projects/' + currentProjectName,
            success: function(res) {
                // Saving response
                $rootScope.pages.project = res;
                $scope.projectContent = res;

                // String escaping
                var sanitizeKeys = ['partners', 'descriptionTop', 'descriptionCut'];
                sanitizeKeys.map(function(key) {
                    $scope.projectContent[key] = $sce.trustAsHtml(res[key]);
                    $scope.projectContent.en[key] = $sce.trustAsHtml(res.en[key]);
                });
            }
        });
    }();

    /*
    * Set all of this into $rootScope
    * */
    var ruPath = $scope.projectContent;
    var enPath = $scope.projectContent.en;
    var isCurrentRU = $rootScope.currLang == 'ru';
    var myKeys = ['style', 'area', 'deadline', 'status', 'title', 'services', 'partners', 'descriptionTop', 'descriptionCut'];

    myKeys.map(function(key) {
        $rootScope.parts.project[key] = isCurrentRU ? ruPath[key] : enPath[key];
    });
};
