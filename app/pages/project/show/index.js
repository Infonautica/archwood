'use strict';

module.exports = angular.module('app.pages.project.show', [])
    .controller('pagesProjectShowCtrl', require('./ctrl'));
