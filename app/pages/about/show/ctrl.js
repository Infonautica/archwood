'use strict';

module.exports = /*@ngInject*/ function($scope, $state, $sce, $rootScope, $http) {

    /*
    * Main object with data
    * */
    $scope.studioContent = {};


    /*
    * Function to get data from server
    * */
    $scope.initialRequest = function() {
        $http({
            method: 'POST',
            url: '/about'
        }).then(
            function successCallback(response) {
                // Saving response
                var res = response.data;
                $scope.studioContent = res;
                $rootScope.pages.about = res;

                // String escaping
                // TODO: For-loop
                $scope.studioContent.about.supervisionText = $sce.trustAsHtml(res.about.supervisionText);
                $scope.studioContent.about.en.supervisionText = $sce.trustAsHtml(res.about.en.supervisionText);

                $scope.studioContent.about.partnersText = $sce.trustAsHtml(res.about.partnersText);
                $scope.studioContent.about.en.partnersText = $sce.trustAsHtml(res.about.en.partnersText);


                // Escaping for vacancies
                for (var i = 0; i < $scope.studioContent.vacancies.length; i++) {
                    var currentVacancy = $scope.studioContent.vacancies[i];
                    currentVacancy.claim = $sce.trustAsHtml(currentVacancy.claim);
                    currentVacancy.responsibilities = $sce.trustAsHtml(currentVacancy.responsibilities);
                }

                /*
                 * Send all of this to the $rootScope
                 * */
                var ruPath = $scope.studioContent.about;
                var enPath = $scope.studioContent.about.en;
                var isCurrentRU = $rootScope.currLang == 'ru';

                // TODO: For-loop
                $rootScope.parts.about.supervisionText = isCurrentRU ? ruPath.supervisionText : enPath.supervisionText;
                $rootScope.parts.about.supervisionHeading = isCurrentRU ? ruPath.supervisionHeading : enPath.supervisionHeading;
                $rootScope.parts.about.partnersText = isCurrentRU ? ruPath.partnersText : enPath.partnersText;
                $rootScope.parts.about.aboutPromo = isCurrentRU ? ruPath.aboutPromo : enPath.aboutPromo;
                $rootScope.parts.about.architectText = isCurrentRU ? ruPath.architectText : enPath.architectText;
                $rootScope.parts.about.architectName = isCurrentRU ? ruPath.architectName : enPath.architectName;
                $rootScope.parts.about.architectPost = isCurrentRU ? ruPath.architectPost : enPath.architectPost;
                $rootScope.parts.about.videos = res.videos;
                $rootScope.parts.about.vacancies = res.vacancies;

                $rootScope.hackLang();
                // -----------------------------------------------------
            }, function errorCallback(response) {
                console.log('Ошибка в получении данных для:\n', response);
            }
        );
    }();
};