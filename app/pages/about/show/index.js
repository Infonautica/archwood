'use strict';

module.exports = angular.module('app.pages.about.show', [])
    .controller('pagesAboutShowCtrl', require('./ctrl'));
