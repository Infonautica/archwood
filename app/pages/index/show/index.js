'use strict';

module.exports = angular.module('app.pages.index.show', [])
    .controller('pagesIndexShowCtrl', require('./ctrl'));
