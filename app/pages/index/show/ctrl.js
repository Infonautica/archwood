'use strict';

module.exports = /*@ngInject*/ function($scope, $state, $rootScope, $timeout, $http) {

    /*
     * Array of works
     * */
    $scope.works = [];

    /*
     * Function to rerender works
     * params:
     * @res - Array
     * */
    $scope.rerenderWorks = function(res) {

        // Clearing array
        $rootScope.parts.home.length = 0;

        // Pushing works into array
        for (var i = 0; i < 9; i++) {
            if (res[i]) {
                $rootScope.parts.home.push(res[i]);
            }
        }

        // Hack to rerender with saving $didgets
        $timeout(function() {
            $scope.$apply(function() {
                $rootScope.parts.home.sort();
            });
        });

        $rootScope.hackLang();
    };

    /*
     * Function to send HTTP-request
     * params:
     * @obj - Object
     * */
    $scope.sendSortRequest = function(obj) {
        $http({
            method: 'POST',
            url: '/projects',
            data: obj
        }).then(function successCallback(response) {
            $rootScope.pages.home = response.data;

            $rootScope.parts.home = [];
            $rootScope.pages.home.map(function(work) {
                var objToPush = {
                    $$hashKey: work.$$hashKey,
                    area: work.area,
                    preview: work.preview,
                    promo: work.promo,
                    releaseDate: work.releaseDate,
                    url: work.url,
                    title: $rootScope.currLang == 'ru' ? work.title : work.en.title
                };

                $rootScope.parts.home.push(objToPush);
            });

            $scope.rerenderWorks(response.data);
        }, function errorCallback(response) {
            console.log('Ошибка в получении портфолио:\n', response);
        });
    };


    /*
     * Initial sort on page load
     * */
    $scope.initialRequest = function() {
        $scope.sendSortRequest($state.params);
    }();
};