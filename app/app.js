'use strict';

var config = require('./config');

var app = require('angular').module('arch', [
		require('angular-ui-router'),
		require('angular-scroll'),
		require('angular-translate'),
		require('./modules/menutop').name,
		require('./modules/actions').name,
		require('./modules/foot').name,
		require('./modules/promo').name,
		require('./modules/works').name,
		require('./modules/description').name,
		require('./modules/gallery').name,
		require('./modules/plans').name,
		require('./modules/sketches').name,
		require('./modules/additional').name,
		require('./modules/about').name,
		require('./modules/supervision').name,
		require('./modules/videos').name,
		require('./modules/publications').name,
		require('./modules/partners').name,
		require('./modules/jobs').name,
		require('./modules/contacts').name,
		require('./modules/map').name,
		require('./modules/service').name,
		require('./modules/projects').name,
		require('./modules/special').name,
		require('./modules/modal').name,
		require('./modules/sidebar').name,
		require('./modules/dropzone').name,
		require('./pages/index').name,
		require('./pages/about').name,
		require('./pages/service').name,
		require('./pages/project').name,
		require('./pages/contacts').name,
		require('./services').name
	])
	.constant('DEBUG', config.debug)
	.constant('langHolder', require('./languages/langHolder'))
	.config(require('./routes'))
	.run(require('./root'));