'use strict';

module.exports = /*@ngInject*/ function($rootScope, $document, menu, $translate, $state, $sce) {

    $rootScope.phones = {
        contact: {
            text: '+7 (495) 545 74 30',
            num: 74955457430
        },
        partners: {
            text: '+7 (499) 322 73 85',
            num: 74993227385
        }
    };

    /*
    * Popups
    * */
    $rootScope.popups = {
        headerWatcher: function() {
            var currentPopup = $('#popup--project');
            currentPopup.find('.w-popup__wrapper--full').on('scroll', function() {
                var distanceY = $('#popup--project').find('.w-popup__wrapper--full').scrollTop(),
                    shrinkOn = 0,
                    headerClass = 'w-popup__navigation--min',
                    headingClass = 'w-popup__heading--margin',
                    header = $('.w-popup__navigation'),
                    heading = header.prev();

                if (distanceY > shrinkOn) {
                    header.addClass(headerClass);
                    heading.addClass(headingClass);
                } else {
                    if (header.hasClass(headerClass)) {
                        header.removeClass(headerClass);
                        heading.removeClass(headingClass);
                    }
                }
            });
        },
        showPopup: function(popupID, isFull) {
            $('body, html').addClass('noscroll');

            var $popup = $('#' + popupID),
                $cur_height = $popup.height(),
                $cur_width = $popup.width(),
                $win_height = $(window).height();
            if (isFull == 'undefined') isFull = false;

            if (popupID == 'popup--thank') {
                $('.w-overlay--dark').fadeIn();
            } else if (popupID == 'popup--project') {
                this.headerWatcher();
            } else {
                $('.w-overlay--white').fadeIn();
            }

            if (isFull) {
                $popup.show()
                    .css({
                        'top': '0',
                        'margin-left': -($cur_width/2),
                        'margin-top': 0
                    })
                    .animate({
                        'top': 0
                    });
            } else {
                $popup.show()
                    .css({
                        'top': -$cur_height,
                        'margin-left': -$cur_width / 2,
                        'margin-top': -$cur_height / 2
                    })
                    .animate({
                        'top': $win_height / 2
                    });
            }

            $popup.focus();
        },
        closePopup: function() {
            $('body, html').removeClass('noscroll');
            $('.w-overlay, .w-popup').fadeOut('100');
        },
        setThis: function($event) {
            var galleryThumb = $event.currentTarget;
            var gallery = galleryThumb.parentNode.parentNode.parentNode;
            var galleryMain = gallery.getElementsByClassName('w-popup__gallery-full')[0];
            var thumbSrc = galleryThumb.src;

            galleryMain.src = thumbSrc;
            gallery.getElementsByClassName('w-popup__gallery-preview--active')[0].classList.remove('w-popup__gallery-preview--active');
            galleryThumb.parentNode.classList.add('w-popup__gallery-preview--active');
        },
        scrollToStep: function(anchor) {
            var offset = $('#' + anchor)[0].offsetTop;
            $('#popup--project .w-popup__wrapper--full:visible').animate({
                scrollTop: offset - ($('.w-popup__navigation').height() + 30)
            }, 400);
        }
    };

    /*
    * Phone mask on inputs
    * */
    $rootScope.mask = function() {
        $(document).ready(function() {
            $('input[type="tel"]').mask('+7 (999) 999-99-99');
        });
    }();



    /*
    * Yandex Metrika counter
    * */
    $rootScope.yaCounter38272390 = new Ya.Metrika({
        id:38272390,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });


    /*
    * Scroll to top on routing
    * */
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState) {
        var IS_NEED_TO_SCROLL_UP = fromState.name != toState.name;

        if (IS_NEED_TO_SCROLL_UP) {
            $('html, body').animate({ scrollTop: 0 }, 0);
        }

        $rootScope.yaCounter38272390.hit(toState.name, {
            referer: window.location.href
        });
    });


    // Hack
    // On page is rendered
    $rootScope.hackLang = function() {
        angular.element(document).ready(function () {
            var portfolioText = $('.header__menulist-link').first().text();
            var langIs = portfolioText == 'Portfolio' ? 'en' : 'ru';
            $rootScope.changeLang(langIs);
        });
    };


    /*
    * Toggle menu on routing
    * */
    $rootScope.stateChangeCounter = 0;
    $rootScope.IS_MENU_OPEN = false;
    $rootScope.$on('$stateChangeSuccess', function() {
        $rootScope.stateChangeCounter++;
        if ($rootScope.stateChangeCounter > 1 && $rootScope.IS_MENU_OPEN) {
            menu.toggleMenu();
        }

        $rootScope.hackLang();
    });


    /*
    * Date field calendar
    * */
    $rootScope.initCalendar = function() {
        $document.ready(function() {
            $('#monthly--1').monthly({
                mode: 'picker',
                target: '#monthly--1__trigger',
                startHidden: true,
                showTrigger: '#monthly--1__trigger',
                stylePast: true,
                disablePast: true,
                weekStart: 'Mon'
            });
        });
    }();


    /*
    * Form validation
    * */
    $rootScope.initValidation = function() {
        $(document).ready(function() {
            jQuery.validate({
                validateOnBlur : true,
                showHelpOnFocus : false,
                addSuggestions : false,
                scrollToTopOnError : false,
                borderColorOnError : '#d71818',
                form : '.validate',
                onSuccess : function(form) {
                    var m_data = form.serialize();
                    var formAction = form.action;
                    console.log(form);
                    console.log($(form));
                    $.ajax({
                        type: "POST",
                        url: '/order',
                        data: m_data,
                        success: function() {
                            $rootScope.popups.closePopup();
                            $rootScope.popups.showPopup('popup--thank', false);
                        }
                    });
                    return false;
                }
            });
        });
    }();

    /*
    * Languages
    * */
    $rootScope.changeLang = function(lang) {
        $translate.use(lang);

        // -------------------------------------------------------
        // Project page
        var ruProjPath = $rootScope.pages.project;
        var enProjPath = $rootScope.pages.project.en;
        var isCurrentRU = lang == 'ru';

        var myKeys = ['style', 'area', 'deadline', 'status', 'title', 'services', 'partners', 'descriptionTop', 'descriptionCut'];
        myKeys.map(function(key) {
            $rootScope.parts.project[key] = isCurrentRU ? ruProjPath[key] : enProjPath[key];
        });

        // -------------------------------------------------------
        // Home page
        $rootScope.parts.home = [];
        $rootScope.pages.home.map(function(work) {
            var objToPush = {
                $$hashKey: work.$$hashKey,
                area: work.area,
                preview: work.preview,
                promo: work.promo,
                releaseDate: work.releaseDate,
                url: work.url,
                title: isCurrentRU ? work.title : work.en.title
            };

            $rootScope.parts.home.push(objToPush);
        });

        $rootScope.parts.home.length = 9; // Cut it up to 9 items

        // -------------------------------------------------------
        // Services
        if ($state.params.key == 'interery') {
            $rootScope.parts.services.interery.layoutProject = {};

            var interiorsKeys = ['description', 'cost', 'title', 'subtitle', 'action'];
            interiorsKeys.map(function(key) {
                $rootScope.parts.services.interery.layoutProject[key] = isCurrentRU ? $rootScope.pages.services.interery.layoutProject[key] : $rootScope.pages.services.interery.en.layoutProject[key];
                $rootScope.parts.services.interery.designProject[key] = isCurrentRU ? $rootScope.pages.services.interery.designProject[key] : $rootScope.pages.services.interery.en.designProject[key];
            });

            var ruPath_INTER = $rootScope.pages.services.interery;
            var enPath_INTER = $rootScope.pages.services.interery.en;

            $rootScope.parts.services.interery.designerSupervision.description = isCurrentRU ? ruPath_INTER.designerSupervision.description : enPath_INTER.designerSupervision.description;
            $rootScope.parts.services.interery.repairSupervision.description = isCurrentRU ? ruPath_INTER.repairSupervision.description : enPath_INTER.repairSupervision.description;
        } else if ($state.params.key == 'soprovozhdenie-remonta') {
            var ruPath = $rootScope.pages.services['soprovozhdenie-remonta'];
            var enPath = $rootScope.pages.services['soprovozhdenie-remonta'].en;

            var repairKeys = ['all_html', 'before_html'];
            repairKeys.map(function(key) {
                $rootScope.parts.services['soprovozhdenie-remonta'].repairSupport[key] = isCurrentRU ? ruPath.repairSupport[key] : enPath.repairSupport[key];
            });

            var repairAfterKeys = ['left', 'right'];
            repairAfterKeys.map(function(key) {
                $rootScope.parts.services['soprovozhdenie-remonta'].repairSupport.after_html[key] = isCurrentRU ? ruPath.repairSupport.after_html[key] : enPath.repairSupport.after_html[key];
            });

            $rootScope.parts.services['soprovozhdenie-remonta'].repairSupport.common_descr = isCurrentRU ? ruPath.repairSupport.common_descr : enPath.repairSupport.common_descr;

        } else if ($state.params.key == 'konsultirovanie') {
            var ruPath_CONSULT = $rootScope.pages.services.konsultirovanie;
            var enPath_CONSULT = $rootScope.pages.services.konsultirovanie.en;

            var consultAdditionalKeys = ['left', 'right'];
            consultAdditionalKeys.map(function(key) {
                $rootScope.parts.services.konsultirovanie.consult.additional[key] = isCurrentRU ? ruPath_CONSULT.consult.additional[key] : enPath_CONSULT.consult.additional[key];
            });

            $rootScope.parts.services.konsultirovanie.consult.description = isCurrentRU ? ruPath_CONSULT.consult.description : enPath_CONSULT.consult.description;
        }

        // -------------------------------------------------------
        // About page

        // TODO: For-loop
        $rootScope.parts.about.supervisionText = isCurrentRU ? $rootScope.pages.about.about.supervisionText : $rootScope.pages.about.about.en.supervisionText;
        $rootScope.parts.about.supervisionHeading = isCurrentRU ? $rootScope.pages.about.about.supervisionHeading : $rootScope.pages.about.about.en.supervisionHeading;
        $rootScope.parts.about.partnersText = isCurrentRU ? $rootScope.pages.about.about.partnersText : $rootScope.pages.about.about.en.partnersText;
        $rootScope.parts.about.aboutPromo = isCurrentRU ? $rootScope.pages.about.about.aboutPromo : $rootScope.pages.about.about.en.aboutPromo;
        $rootScope.parts.about.architectText = isCurrentRU ? $rootScope.pages.about.about.architectText : $rootScope.pages.about.about.en.architectText;
        $rootScope.parts.about.architectName = isCurrentRU ? $rootScope.pages.about.about.architectName : $rootScope.pages.about.about.en.architectName;
        $rootScope.parts.about.architectPost = isCurrentRU ? $rootScope.pages.about.about.architectPost : $rootScope.pages.about.about.en.architectPost;

        // Iterable arays
        if ($state.current.name == 'about') {
            // Videos
            var vids = $rootScope.pages.about.videos;
            var newVids = [];

            // Re-create videos
            for (var i = 0; i < vids.length; i++) {
                var newVidObj = {
                    link: vids[i].link,
                    preview: vids[i].preview,
                    title: isCurrentRU ? vids[i].title : vids[i].en.title,
                    description: isCurrentRU ? vids[i].description : vids[i].en.description
                };
                if (vids[i]) {
                    newVids.push(newVidObj);
                }
            }

            // -------------------------------
            // Vacancies
            var vacs = $rootScope.pages.about.vacancies;
            var newVacs = [];

            // Re-create vacancies
            for (var u = 0; u < vacs.length; u++) {
                var newVacObj = {
                    vacancie: vacs[u].vacancie,
                    name: isCurrentRU ? vacs[u].name : vacs[u].en.name,
                    responsibilities: isCurrentRU ? $sce.trustAsHtml(vacs[u].responsibilities) : $sce.trustAsHtml(vacs[u].en.responsibilities),
                    claim: isCurrentRU ? $sce.trustAsHtml(vacs[u].claim) : $sce.trustAsHtml(vacs[u].en.claim)
                };
                if (vacs[u]) {
                    newVacs.push(newVacObj);
                }
            }

            newVids.sort();
            $rootScope.parts.about.videos = newVids;
            $rootScope.parts.about.vacancies = newVacs;
        }
    };

    /*
    * Method change page and scroll to ID
    * params:
    * @page - String
    * @eID - String
    * */
    $rootScope.goToHash = function(page, eID) {
        $state.go(page).then(function() {
            var targetElem = $('#arch').find('#' + eID)[0];
            setTimeout(function() {
                var offset = targetElem.offsetTop;
                if (page == 'about' && eID == 'partners') {
                    offset = (offset - 110);
                } else {
                    offset = (offset - $('#header').outerHeight())
                }

                $('html, body').stop().animate({
                    scrollTop: offset
                }, 400);

                $('.w-popup:visible').focus();
            }, 500);
        });
    };


    $rootScope.initIphoneScroll = function() {
        angular.element(document).ready(function() {
            if (/iPhone|iPod|iPad/.test(navigator.userAgent)) {
                $('.arch').wrap(function () {
                    var $this = $(this);
                    return $('<div />').css({
                        width: $this.attr('width'),
                        height: $this.attr('height'),
                        overflow: 'auto',
                        '-webkit-overflow-scrolling': 'touch'
                    });
                });
            }
        });
    }();


    /*
    * Partial content
    * */
    $rootScope.currLang = $translate.use();
    $rootScope.pages = {
        project: {
            style: 'style1',
            area: 'area1',
            deadline: 'deadline1',
            status: 'status1',
            title: 'title1',
            services: 'services1',
            partners: 'partners1',
            descriptionTop: 'descriptionTop1',
            descriptionCut: 'descriptionCut1',
            en: {
                style: 'style1',
                area: 'area1',
                deadline: 'deadline1',
                status: 'status1',
                title: 'title1',
                services: 'services1',
                partners: 'partners1',
                descriptionTop: 'descriptionTop1',
                descriptionCut: 'descriptionCut1'
            }
        },
        home: [],
        services: {
            interery: {
                consult: 'consult1',
                designProject: 'designProject1',
                designerSupervision: 'designerSupervision1',
                layoutProject: 'layoutProject',
                repairSupervision: 'repairSupervision',
                repairSupport: 'repairSupport',
                en: {}
            },
            'soprovozhdenie-remonta': {},
            konsultirovanie: {}
        },
        about: {
            about: {
                supervisionText: 'supervisionText1',
                en: {
                    supervisionText: 'supervisionText1'
                }
            }
        }
    };

    $rootScope.parts = {
        project: {
            style: 'style1',
            area: 'area1',
            deadline: 'deadline1',
            status: 'status1'
        },
        home: {},
        services: {
            interery: {
                layoutProject: {
                    description: 'description1',
                    cost: 'cost1',
                    title: 'title1',
                    subtitle: 'subtitle1'
                },
                designProject: {
                    description: 'description1',
                    cost: 'cost1',
                    title: 'title1',
                    subtitle: 'subtitle1'
                },
                designerSupervision: {
                    description: 'description1'
                },
                repairSupervision: {
                    description: 'description1'
                }
            },
            'soprovozhdenie-remonta': {
                repairSupport: {
                    all_html: 'all_html',
                    before_html: 'before_html',
                    after_html: {
                        left: 'left1',
                        right: 'right1'
                    }
                }
            },
            konsultirovanie: {
                consult: {
                    description: 'description1',
                    additional: {
                        left: 'left1',
                        right: 'right1'
                    }
                }
            }
        },
        about: {
            about: {
                supervisionText: 'supervisionText1',
                en: {
                    supervisionText: 'supervisionText1'
                }
            },
            videos: []
        }
    };
};