// Assets
var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var images = require('gulp-image-optimization');
var jade = require('gulp-jade');
var gulpExec = require('gulp-exec');

// Angular stuff
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var stringify = require('stringify');

gulp.task('bro', function() {
    gulp.src('./gulpfile.js').pipe(gulpExec('browserify -t stringify app/app.js -o app/bundle.js'));
});

// -------------------------------------------------------

// Default task
gulp.task('default', ['minify', 'scripts', 'jade'] ,function() {});

// Watch
gulp.task('watch', ['default'], function() {
    console.log('Gulp is watching you...');
    gulp.watch("./resources/scss/**", ['minify']);
    gulp.watch("./resources/js/**", ['scripts']);
    gulp.watch("./resources/pages/**", ['jade']);
    gulp.watch("./resources/elements/**", ['jade']);
    // gulp.watch("./app/**", ['bro']);
});

// -------------------------------------------------------

// SCSS compiling
gulp.task('sass', function () {
	return gulp.src('./resources/scss/main.scss')
			.pipe(sass.sync().on('error', sass.logError))
			.pipe(gulp.dest('./public/css'));
});

// Auto-prefixer
gulp.task('prefix', ['sass'], function () {
    return gulp.src('./public/css/main.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./public/css'));
});

// CSS Minifying
gulp.task('minify', ['prefix'], function() {
	return gulp.src('./public/css/main.css')
			.pipe(minifyCss({compatibility: 'ie8'}))
			.pipe(rename('./public/css/main.min.css'))
			.pipe(gulp.dest('./'));
});

// Script concatenation
gulp.task('scripts', ['scripts--vendor', 'scripts--custom'], function() {});

gulp.task('scripts--vendor', function() {
	return gulp.src('./resources/js/vendor/*.js')
	    .pipe(concat('vendor.js'))
	    .pipe(gulp.dest('./public/js/'));
});

gulp.task('scripts--custom', function() {
	return gulp.src('./resources/js/custom/*.js')
	    .pipe(concat('custom.js'))
	    .pipe(gulp.dest('./public/js/'));
});

// Jade to HTML
gulp.task('jade', function() {
  var YOUR_LOCALS = {};
 
  gulp.src('./resources/pages/*.jade')
    .pipe(jade({
      locals: YOUR_LOCALS
    }))
    .pipe(gulp.dest('./public/html/'));
});

// Images optimization
gulp.task('images', function(cb) {
    gulp.src(['./resources/img/**/*.png','./resources/img/**/*.jpg','./resources/img/**/*.gif','./resources/img/**/*.jpeg']).pipe(images({
        optimizationLevel: 5,
        progressive: true,
        interlaced: true
    })).pipe(gulp.dest('./public/img')).on('end', cb).on('error', cb);
});